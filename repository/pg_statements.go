package repository

const (
	insertNewCountry = `INSERT INTO common.country(id, name, iso_code)
					VALUES(:id, :name, :isocode)`
	insertNewTown = `INSERT INTO common.town(name, country_id, longitude, latitude, time_zone_code)
					VALUES(:name, :countryid, :longitude, :latitude, :timezonecode) 
					RETURNING id`
	insertNewDistrict = `INSERT INTO common.district(name, town_id)
					VALUES(:name, :townid) 
					RETURNING id`

	getCountries = `SELECT * FROM common.country`

	getTowns = `select t.id as "id", t.name as "name", d.id as "district_id", d.name as "district_name", t.time_zone_code as "time_zone_code"
				from common.town t
				left join common.district d on t.id = d.town_id
				where t.country_id = $1`

	getLocalizations = `SELECT d.id, c.id as "code", CONCAT(c.name, ' ', t.name, ' ', d.name) as position
						from common.district d
						inner join common.town t on t.id = d.town_id 
						inner join common.country c on c.id = t.country_id `
)

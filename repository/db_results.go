package repository

import "github.com/google/uuid"

type CountryResult struct {
	ID      string    `db:"id"`
	Name    string    `db:"name"`
	IsoCode string    `db:"iso_code"`
	UID     uuid.UUID `db:"uid"`
}

type TownResult struct {
	ID           uuid.UUID `db:"id"`
	Name         string    `db:"name"`
	DistrictID   uuid.UUID `db:"district_id"`
	DistrictName string    `db:"district_name"`
	TimeZoneCode string    `db:"time_zone_code"`
}

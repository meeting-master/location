package repository

import (
	"gitlab.com/meeting-master/connector/pgsql"
	"gitlab.com/meeting-master/location/model"
)

type LocationRepository interface {
	NewCountry(request model.CountryRequest) error

	NewTown(request model.TownRequest) (string, error)

	NewDistrict(request model.DistrictRequest) (string, error)

	Countries() ([]CountryResult, error)

	Towns(countryId int) ([]TownResult, error)

	Localizations() ([]model.Localization, error)
}

type repository struct {
	db *pgsql.DB
}

func NewLocationRepository(db *pgsql.DB) LocationRepository {
	return repository{db: db}
}

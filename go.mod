module gitlab.com/meeting-master/location

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/lib/pq v1.5.2 // indirect
	github.com/monchemin/C-19-API v0.0.0-20200509202517-b6117244d2c5
	gitlab.com/meeting-master/connector v0.0.0-20200516034010-6e91906a87d6
)

package service

import (
	"github.com/google/uuid"
	"gitlab.com/meeting-master/connector/errors"
	"gitlab.com/meeting-master/location/model"
	"strconv"
)

func (ls locationService) NewCountry(request model.CountryRequest) error {
	if !request.IsValid() {
		return errors.InvalidRequestData()
	}

	return ls.repository.NewCountry(request)
}

func (ls locationService) NewTown(request model.TownRequest) (string, error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}

	return ls.repository.NewTown(request)
}

func (ls locationService) NewDistrict(request model.DistrictRequest) (string, error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}

	return ls.repository.NewDistrict(request)
}

func (ls locationService) Countries() ([]model.Country, error) {
	countryInfos, err := ls.repository.Countries()
	if err != nil {
		return nil, err
	}

	if len(countryInfos) == 0 {
		return nil, errors.EmptyResultData()
	}

	countries := make([]model.Country, len(countryInfos))
	for index, info := range countryInfos {
		countries[index] = model.Country{
			ID:      info.ID,
			Name:    info.Name,
			IsoCode: info.IsoCode,
		}
	}
	return countries, err
}

func (ls locationService) Towns(countryCode string) ([]model.Town, error) {
	id, err := strconv.Atoi(countryCode)
	if err != nil {
		return nil, errors.InvalidRequestData()
	}
	tws, err := ls.repository.Towns(id)

	if err != nil {
		return nil, err
	}

	if len(tws) == 0 {
		return nil, nil
	}

	tm := make(map[uuid.UUID]model.Town)

	for _, v := range tws {
		d := model.District{
			ID:   v.ID.String(),
			Name: v.DistrictName,
		}

		if _, ok := tm[v.ID]; !ok {
			tm[v.ID] = model.Town{
				ID:        v.ID.String(),
				Name:      v.Name,
				Districts: nil,
			}
		}
		t := tm[v.ID]
		t.Districts = append(t.Districts, d)
		tm[v.ID] = t
	}

	towns := make([]model.Town, len(tm))

	for _, v := range tm {
		towns = append(towns, v)
	}
	return towns, nil
}

func (ls locationService) Localizations() ([]model.Localization, error) {
	return ls.repository.Localizations()
}

package service

import (
	"gitlab.com/meeting-master/location/model"
	"gitlab.com/meeting-master/location/repository"
)

type LocationService interface {
	NewCountry(request model.CountryRequest) error

	NewTown(request model.TownRequest) (string, error)

	NewDistrict(request model.DistrictRequest) (string, error)

	Countries() ([]model.Country, error)

	Towns(countryCode string) ([]model.Town, error)

	Localizations() ([]model.Localization, error)
}

type locationService struct {
	repository repository.LocationRepository
}

func NewLocationService(repo repository.LocationRepository) LocationService {
	return &locationService{repository: repo}
}
